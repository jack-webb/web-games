apiVersion: v1
kind: Template
metadata:
  name: web-games
  annotations:
    iconClass: pficon-home
    tags: web app
    template.openshift.io/provider-display-name: "LucaVazzano.eu"


parameters:
- name: APP_NAME
  description: The name of the app inside OpenShift
  displayName: App Name
  value: web-games
  required: true
- name: GITLAB_WEBHOOK_SECRET
  description: A secret string used to configure the GitLab webhook.
  displayName: GitLab Webhook Secret
  required: true
  from: '[a-zA-Z0-9]{40}'
  generate: expression


labels:
  app: ${APP_NAME}
  app.kubernetes.io/component: ${APP_NAME}
  app.kubernetes.io/instance: ${APP_NAME}


objects:

# Image Streams

- apiVersion: v1
  kind: ImageStream
  metadata:
    name: ${APP_NAME}-build
  spec: {}

- apiVersion: v1
  kind: ImageStream
  metadata:
    name: ${APP_NAME}-runtime
  spec: {}

# Build Config

- apiVersion: v1
  kind: BuildConfig
  metadata:
    name: ${APP_NAME}-build
  spec:
    output:
      to:
        kind: ImageStreamTag
        name: ${APP_NAME}-build:latest
    postCommit: {}
    resources:
      requests:
        cpu: 100m
        memory: 500Mi 
      limits:
        cpu: 100m
        memory: 500Mi 
    source:
      git:
        uri: https://gitlab.com/LucaVazz/web-games.git
        ref: master
      contextDir: "."
      type: Git
    strategy:
      sourceStrategy:
        from:
          kind: DockerImage
          name: docker.io/nodeshift/ubi8-s2i-web-app:10.x
        pullSecret:
          name: "docker-hub-pull-secret"
        env:
        - name: OUTPUT_DIR
          value: .
        - name: YARN_ENABLED
          value: "true"
        incremental: true
      type: Source
    triggers:
    - type: ConfigChange
    - type: GitLab
      gitlab:
        secret: ${GITLAB_WEBHOOK_SECRET}

- apiVersion: v1
  kind: BuildConfig
  metadata:
    name: ${APP_NAME}-runtime
  spec:
    output:
      to:
        kind: ImageStreamTag
        name: ${APP_NAME}-runtime:latest
    postCommit: {}
    resources: {}
    source:
      type: Image
      images:
      - from:
          kind: ImageStreamTag
          name: ${APP_NAME}-build:latest
        paths:
        - destinationDir: .
          sourcePath: /opt/app-root/output/.
    strategy:
      sourceStrategy:
        from:
          kind: DockerImage
          name: registry.access.redhat.com/ubi8/python-38
        env:
        - name: APP_FILE
          value: "run-server.py"
        incremental: true
      type: Source
    triggers:
    - type: ConfigChange
    - type: ImageChange
      imageChange:
        from:
          kind: ImageStreamTag
          name: ${APP_NAME}-build:latest

# Deployment Config

- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    name: ${APP_NAME}
  spec:
    replicas: 1
    revisionHistoryLimit: 2
    selector:
      app: ${APP_NAME}
    strategy:
      rollingParams:
        timeoutSeconds: 3600
      type: Rolling
    template:
      metadata:
        labels:
          app: ${APP_NAME}
          app.kubernetes.io/component: ${APP_NAME}
          app.kubernetes.io/instance: ${APP_NAME}
      spec:
        containers:
        - name: ${APP_NAME}-runtime
          imagePullPolicy: IfNotPresent
          image: ${APP_NAME}-runtime
          resources:
            requests:
              cpu: 250m
              memory: 256Mi
            limits:
              cpu: 750m
              memory: 256Mi
          ports:
          - name: http
            containerPort: 5000
          livenessProbe:
            httpGet:
              port: http
              path: /health
            initialDelaySeconds: 30
          readinessProbe:
            httpGet:
              port: http
              path: /health
            initialDelaySeconds: 10
          volumeMounts:
          - name: settings-volume
            mountPath: /opt/app-root/src/app/settings/data
          - name: sos-data-volume
            mountPath: /opt/app-root/src/app/game_logic/sos/data
        volumes:
        - name: settings-volume
          configMap:
            name: web-games-settings
            items:
            - key: settings
              path: settings.yaml
        - name: sos-data-volume
          configMap:
            name: web-games-sos-data
            items:
            - key: decks
              path: decks.yaml
        restartPolicy: Always
    triggers:
    - type: ConfigChange
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
          - ${APP_NAME}-runtime
        from:
          kind: ImageStreamTag
          name: '${APP_NAME}-runtime:latest'

# Service

- apiVersion: v1
  kind: Service
  metadata:
    name: ${APP_NAME}
  spec:
    ports:
    - name: http
      port: 80
      targetPort: 5000
    selector:
      app: ${APP_NAME}

# Routes

- apiVersion: v1
  kind: Route
  metadata:
    name: ${APP_NAME}
  spec:
    port:
      targetPort: http
    tls:
      termination: edge
      insecureEdgeTerminationPolicy: Redirect
    to:
      kind: Service
      name: ${APP_NAME}

- apiVersion: v1
  kind: Route
  metadata:
    name: ${APP_NAME}-externally
    annotations:
      kubernetes.io/tls-acme: "true"
  spec:
    host: games.lucavazzano.eu
    port:
      targetPort: http
    tls:
      termination: edge
      insecureEdgeTerminationPolicy: Redirect
    to:
      kind: Service
      name: ${APP_NAME}
