/**
 * Manages copying the frontend dependencies into the static folder.
 */

const { src, dest, watch } = require('gulp')
const rename = require('gulp-rename')
const sass = require('gulp-sass')
sass.compiler = require('sass')


function copyFrontendDependencies() {
    /** globs to describe what files to copy. relative to the node_modules folder. */
    const srcGlobs = [
        'line-awesome/dist/line-awesome/fonts/la-*-*.*',
        'vue/dist/vue.min.js',
        'socket.io-client/dist/socket.io.min.js',
        'frappe-charts/dist/frappe-charts.min.iife.js',
    ]
    const destPath = '../app/static/libs/'

    return src(srcGlobs.map(p => `../node_modules/${p}`))
        .pipe(dest(destPath))
    ;
}
exports.copyFrontendDependencies = copyFrontendDependencies

function buildStyles() {
    const srcGlobs = [
        'style-global/global.sass',
        'templates/**/*.sass'
    ]
    const destPath = '../app/static/styles'

    return src(srcGlobs.map(p => `../app/${p}`))
        .pipe(
            sass({
                outputStyle: 'compressed'
            }).on('error', sass.logError)
        )
        .pipe(
            rename(function (path) {
                path.dirname = ''
            })
        )
        .pipe(dest(destPath))
    ;
}
exports.buildStyles = buildStyles

function buildStylesWatch() {
    watch('../app/**/*.sass', buildStyles)
}
exports.buildStylesWatch = buildStylesWatch
