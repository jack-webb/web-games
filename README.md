# Web-Games

[Play some games. With your Friends.](https://youtu.be/jL3U7afEVGc?t=33s) With login via Discord.


## Project Overview
- `app`: everything that runs the actual app, see README inside the directory for more details
- `deployment`: definitions for the deployment to OpenShift
- `scripts`: helper scripts, use when noted here


## Local Dev-Usage

#### basic set-up
1. verify that Yarn v1 (classic) is available
1. install and set-up frontend requirements with `yarn && yarn build`
1. verify that Python v3.6 or later is available
1. install backend requirements with `pip install -r requirements-dev.txt`
1. compile the translations by calling `sh ./scripts/babel_compile.sh`
1. verify that MongoDB v2.4 or later (later major is fine as well) is installed
1. copy `app/settings/data/settings_example.yaml` to `app/settings/data/settings.yaml` and fill out the values appropriately

#### set-up for a minimal local run
1. generate a self-signed SSL certificate and place `ssl.cert` and `ssl.key` in the root directory
    (for example use [OpenSSL](https://superuser.com/a/126165/934856))
1. start the local server with `python run-local.py`
    - Please note that this will not actually use web-sockets, it falls back to socket.io's polling implementation. This is due to eventlet not coping with SSL directly but needing a proxy in-front, which is a bit much for local development. In effect it works the same due to socket.io's abstractions.

#### set-up for a production-like local run
1. install backend requirements for actual Web-Socket connections with `pip install -r requirements-prod.txt`
1. set-up a reverse-proxy with i.e. nginx to tunnel to `127.0.0.1:5000` and to provide SSL for the connections
1. start the server with `python run-server.py`

#### helpers for development
- to update the style:
    1. edit the sass-file(s) as needed inside the `app/style-global` and/or `app/templates` folder
    1. run `yarn buildStyles` to have the changes applied
        - no need to commit the build output, it gets re-generated as needed
        - alternatively, have `yarn buildStyles:watch` running in a terminal in a background to continuously re-build the styles

- to update translations:
    1. run `./babel-extract.sh` to update the translation data
    1. fill in the blanks in `translations\de\LC_MESSAGES\messages.po` and `translations\en\LC_MESSAGES\messages.po`
    1. run `./babel-compile.sh` to optimize the translation data
    1. commit the changes to the `.po` and `.mo` files


## CI / CD

#### GitLab CI

The GitLab CI is used according to the `.gitlab-ci.yml` for running merge checks on all non-master branches to ensure a smooth integration of code changes.

## OpenShift CD

The definitions for the deployment to OpenShift are placed inside the `deployment-template.yaml` file.

This file only get applied manually by running `oc process -f deployment-template.yaml | oc apply -f -`.

Deployments get triggered by new code being integrated into the master branch.


## Contributing

I'm open for all Forks, Feedback and Pull Requests ;)

#### Commit Convention

The commit message should be structured as follows:
```
<type>([optional scope]): <description>

[optional description]

[optional issue reference]
```

**type** can be one of:
- `game`: initial work for a new game
- `feat`: improves an existing game or function
- `fix`: fixes a bug
- `refactor`: improves existing code without changing user-facing features
- `doc`: improves documentation
- `build`: improves the build setup (local and CI/CD)

**scope** is most if the time the game or just empty brackets

The description holds additional details which are too long to fit in the first line.

The issue reference should comply to [GitLab's format](https://docs.gitlab.com/ce/user/project/issues/managing_issues.html#closing-issues-automatically)  
or be like `relates to #<issue-id>`.

For example:
```
doc(): add section about the Commit Convention to the README
```

```
fix(CAH): fixed some card texts

fixes #1337
```

```
feat(): add option to kick players

only available for initiator from the player list

relates to #4242
```


## License

This project is licensed under the terms of the *GNU General Public License v3.0*. For further information, please look [here](http://choosealicense.com/licenses/gpl-3.0/) or [here<sup>(DE)</sup>](http://www.gnu.org/licenses/gpl-3.0.de.html).
