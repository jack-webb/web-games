# This requirements file is used for deployment


-r './requirements-base.txt'

eventlet==0.25.*  # local dev setup is too complicated with self-signed SSL + eventlet
sentry-sdk[flask]==0.14.*  # reduce log spam during dev by only activating Sentry in prod setup
