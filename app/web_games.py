import random

from flask import Flask
from flask_socketio import SocketIO

from . import init, endpoint_mapping
from .settings.settings_resolver import Settings


def startup() -> (Flask, SocketIO):
    random.seed()

    settings = Settings()

    init.configure_logging()
    init.configure_sentry(settings)

    app = init.configure_app(settings)

    init.configure_db(app, settings)
    init.configure_i18n(app)
    init.configure_request_logging(app)
    init.configure_bots_redirect(app)

    socketio_app = init.configure_socketio_app(app, settings)

    endpoint_mapping.configure_http_endpoints(app, settings)
    endpoint_mapping.configure_ws_endpoints(socketio_app)

    return app, socketio_app
