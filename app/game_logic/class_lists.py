from .bingo.bingo_game import BingoGame
from .bingo.bingo_match import BingoMatch
from .bingo.model.bingo_player_state import BingoPlayerState
from .cah.cah_game import CahGame
from .cah.cah_match import CahMatch
from .cah.model.cah_player_state import CahPlayerState
from .elect.elect_game import ElectGame
from .elect.elect_match import ElectMatch
from .elect.model.elect_player_state import ElectPlayerState
from .sos.model.sos_player_state import SosPlayerState
from .sos.sos_game import SosGame
from .sos.sos_match import SosMatch


def list_game_classes():
    return [
        CahGame,
        SosGame,
        BingoGame,
        ElectGame
    ]


def get_supported_game_classes(locale: str):
    return [
        gc
        for gc in list_game_classes()
        if locale in gc.get_supported_locales()
    ]


def find_game_class(internal_name: str):
    for game in list_game_classes():
        if game.get_internal_name() == internal_name:
            return game

    raise ValueError('A game with the internal name "%s" is not known' % internal_name)


def list_match_classes():
    return [
        CahMatch,
        SosMatch,
        BingoMatch,
        ElectMatch
    ]


def list_player_state_classes():
    return [
        CahPlayerState,
        SosPlayerState,
        BingoPlayerState,
        ElectPlayerState
    ]
