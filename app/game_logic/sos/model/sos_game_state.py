from enum import Enum


class SosGameState(Enum):
    STARTING = 'STARTING'
    SELECTING_TARGET = 'SELECTING_TARGET'
    ANSWERING_ONE = 'ANSWERING_ONE'
    ANSWERING_ALL = 'ANSWERING_ALL'
    DONE = 'DONE'
    # we don't use FINISHED for this match as it has no scores, so we don't want the default results screen which is
    #  triggered to be displayed by the FINISHED state
