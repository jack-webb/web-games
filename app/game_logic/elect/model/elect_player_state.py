from mongoengine import ReferenceField

from app.model.user import User
from ...base.model.base_player_state import BasePlayerState


class ElectPlayerState(BasePlayerState):
    vote = ReferenceField(User)
