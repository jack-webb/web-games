from typing import Union, Optional

from mongoengine import IntField, ListField, ReferenceField
from werkzeug.datastructures import ImmutableMultiDict

from app.game_logic.base.base_match import BaseMatch
from app.model.user import User
from app.util.error.bad_user_request_error import BadUserRequestError
from app.util.mongoengine_fields import StringEnumField
from .model.elect_game_state import ElectGameState
from .model.elect_player_state import ElectPlayerState


class ElectMatch(BaseMatch):

    # region Fields
    points_to_win = IntField(required=True)
    game_state = StringEnumField(ElectGameState, required=True)
    player_states = ListField(ReferenceField('ElectPlayerState'))
    # endregion

    # region Class Methods
    @classmethod
    def get_internal_name(cls) -> str:
        return "elect"
    # endregion

    # region Constructor
    def _gl_init(self, **kwargs) -> None:
        self.points_to_win = kwargs['points_to_win']
        self.game_state = ElectGameState.STARTING
    # endregion

    # region Event Handlers
    def _gl_handle_join_init_player_state(self, user: User) -> ElectPlayerState:
        return ElectPlayerState(vote=None)

    def _gl_handle_join(self, user: User) -> None:
        if self.game_state == ElectGameState.VOTING:
            self._emit_ws_state_change()

    def _gl_handle_disconnect(self, user: User) -> None:
        if self.game_state == ElectGameState.VOTING:
            if self.__calc_is_vote_finished():
                self.__update_state_for_results()
            else:
                self._emit_ws_state_change()

    def _gl_handle_rejoin(self, user: User, player_state: ElectPlayerState) -> None:
        if self.game_state == ElectGameState.VOTING:
            self._emit_ws_state_change()

    def _gl_handle_play(self, user: User, data: ImmutableMultiDict) -> None:
        action = data.get('action')
        if action is None:
            raise BadUserRequestError('no action')

        is_lead = user == self.initiator

        if action == 'start':
            if not is_lead:
                raise BadUserRequestError('invalid action - must be initiator')
            if self.game_state != ElectGameState.STARTING:
                raise BadUserRequestError('invalid action - must be in STARTING state')

            self.__update_state_for_new_round()

        elif action == 'vote_selected':
            if self.game_state != ElectGameState.VOTING:
                raise BadUserRequestError('invalid action - must be in VOTING state')

            try:
                index = int(data.get('index'))
            except BaseException as ex:
                raise BadUserRequestError(f'invalid action - invalid index passed ({str(ex)})')
            voted_player = self.players[index]

            player_state = self.get_player_state(user)
            player_state.modify(set__vote=voted_player)

            if self.__calc_is_vote_finished():
                self.__update_state_for_results()
            else:
                self._emit_ws_state_change(user, player_state)

        elif action == 'show_result_details':
            if not is_lead:
                raise BadUserRequestError('invalid action - must be initiator')
            if self.game_state != ElectGameState.RESULT_SUMMARY:
                raise BadUserRequestError('invalid action - must be in RESULT_SUMMARY state')

            self.modify(set__game_state=ElectGameState.RESULT_DETAILS)
            self._emit_ws_state_change()

        elif action == 'next_round':
            if not is_lead:
                raise BadUserRequestError('invalid action - must be initiator')
            if self.game_state not in [ElectGameState.RESULT_SUMMARY, ElectGameState.RESULT_DETAILS]:
                raise BadUserRequestError('invalid action - must be in RESULT_SUMMARY or RESULT_DETAILS state')

            self._advance_round_number()
            self.__update_state_for_new_round()

        else:
            raise BadUserRequestError(f'bad action `{action}`')

    def __update_state_for_new_round(self):
        self.modify(set__game_state=ElectGameState.VOTING)

        for ps in self.player_states:
            ps.modify(set__vote=None)

        self._emit_ws_state_change()

    def __update_state_for_results(self):
        vote_data = self.__calc_vote_data()
        voter_count = len(vote_data)
        vote_count_1st = len(vote_data[0][1])
        vote_count_2nd = len(vote_data[1][1]) if voter_count > 1 else 0

        if vote_count_1st == vote_count_2nd:
            # no points this round
            stale_results = self.results
            is_finished = False
        else:
            winning_player = vote_data[0][0]
            winning_pr = next(r for r in self.results if r.player == winning_player)
            winning_new_points = winning_pr.points_history[-1] + 1
            winning_pr.modify(push__points_history=winning_new_points)

            is_finished = winning_new_points == self.points_to_win

            stale_results = [r for r in self.results if r.player != winning_player]

        for result in stale_results:
            result.modify(push__points_history=result.points_history[-1])

        if is_finished:
            sorted_results = sorted(self.results, key=lambda res: res.points_history[-1], reverse=True)
            self.modify(set__results=sorted_results)
            self.modify(set__game_state=ElectGameState.FINISHED)
            self._emit_ws_finished()
            return

        if vote_count_1st == voter_count:
            self.modify(set__game_state=ElectGameState.RESULT_DETAILS)
        else:
            self.modify(set__game_state=ElectGameState.RESULT_SUMMARY)
            self._emit_ws_player_change()

        self._emit_ws_state_change()
    # endregion

    # region View Renderers
    def _gl_render_inner_view_for_state(self, user: User, player_state: ElectPlayerState = None) -> Union[str, dict]:
        is_lead = user == self.initiator

        if self.game_state == ElectGameState.STARTING:
            return self._render_base_state_template(
                'starting', user, user_is_initiator=(user == self.initiator)
            )

        elif self.game_state == ElectGameState.VOTING:
            if player_state is None:
                player_state = self.get_player_state(user)
            return self._render_state_template(
                'voting', user, players=self.players, voted_for=player_state.vote
            )

        elif self.game_state == ElectGameState.RESULT_SUMMARY:
            return self._render_state_template(
                'results', user, is_lead=is_lead,
                vote_data=self.__calc_vote_data(), hide_voters=True
            )

        elif self.game_state == ElectGameState.RESULT_DETAILS:
            return self._render_state_template(
                'results', user, is_lead=is_lead,
                vote_data=self.__calc_vote_data(), hide_details_button=True
            )

        else:
            raise RuntimeError(f'cannot render state `{self.game_state}`')

    def _gl_get_player_tags(self, user: User) -> [str]:
        return []
    # endregion

    # region Internal Helpers
    def __calc_is_vote_finished(self) -> bool:
        for p in self.players:
            ps: Optional[ElectPlayerState] = self.get_player_state(p)
            if ps is not None and ps.vote is None:
                return False
        return True

    def __calc_vote_data(self) -> [(User, [User])]:
        result_map = {
            p: [] for p in self.players
        }

        for p in self.players:
            ps: Optional[ElectPlayerState] = self.get_player_state(p)
            if ps is not None:
                result_map[ps.vote].append(ps.player)

        result = [
            (p, v) for p, v in result_map.items()
        ]
        result.sort(key=lambda d: len(d[1]), reverse=True)

        return result
    # endregion
