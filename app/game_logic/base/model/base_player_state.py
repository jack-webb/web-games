from datetime import datetime

from mongoengine import Document, ReferenceField, StringField, DateTimeField

from app.model.user import User


class BasePlayerState(Document):
    player = ReferenceField(User, required=True)
    sid = StringField(required=True)
    created = DateTimeField(default=datetime.utcnow)

    meta = {
        'allow_inheritance': True,
        'indexes': [
            {'fields': ['created'], 'cls': False, 'expireAfterSeconds': 48 * 60 * 60}
        ]
    }
