from random import sample
from typing import Optional

from .abstract_bingo_service import AbstractBingoService


class UsBingoService(AbstractBingoService):
    @classmethod
    def get_card_size(cls) -> (int, int):
        """
        Get the card size for the specific bingo variant.
        :return: A tuple row_length, row_count
        """
        return 5, 5

    @classmethod
    def get_max_number(cls) -> int:
        """
        Get the highest possible number that can be called out
        """
        return 75

    @classmethod
    def generate_card(cls) -> [[Optional[int]]]:
        """
        Generates a random card for the specific bingo variant.
        :return: A two dimensional array representing the card

        Example:
             1  2  3  4  5
             6  7  8  9 10
            11 12    13 14
            15 16 17 18 19
            20 21 22 23 24
        """
        (row_length, row_count) = cls.get_card_size()
        numbers_count = row_length * row_count

        numbers = sample(range(1, cls.get_max_number() + 1), numbers_count)
        card = [numbers[i:i+row_length] for i in range(0, numbers_count, row_length)]

        card[2][2] = None
        return card

    @classmethod
    def check_win(cls, card: [[Optional[int]]], marked: [[bool]]) -> bool:
        """
        Checks for a given card if it is marked in a way that counts as a win.
        :param card: A two dimensional array representing the numbers on the card
        :param marked: A two dimensional array, representing what fields are marked already
        :return: If its a win
        """

        (row_length, row_count) = cls.get_card_size()

        for iv in range(row_count):
            horizontal_win = True
            for ih in range(row_length):
                if card[iv][ih] is None or not marked[iv][ih]:
                    horizontal_win = False
                    break
            if horizontal_win:
                return True

        for ih in range(row_length):
            vertical_win = True
            for iv in range(row_count):
                if card[iv][ih] is None or not marked[iv][ih]:
                    vertical_win = False
                    break
            if vertical_win:
                return True

        return False
