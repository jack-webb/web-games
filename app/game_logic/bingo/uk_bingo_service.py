from typing import Optional

from .abstract_bingo_service import AbstractBingoService


class UkBingoService(AbstractBingoService):
    @classmethod
    def get_card_size(cls) -> (int, int):
        """
        Get the card size for the specific bingo variant.
        :return: A tuple rowNone,length, rowNone,count
        """
        return 9, 3

    @classmethod
    def get_max_number(cls) -> int:
        """
        Get the highest possible number that can be called out
        """
        return 90

    @classmethod
    def generate_card(cls) -> [[Optional[int]]]:
        """
        Generates a random card for the specific bingo variant.
        :return: A two dimensional array representing the card
        """
        # TODO: properly implement and remove mock
        return [
              [5,  None,  None,  None, 49, None, 63, 75, 80],
              [None,  None, 28, 34,  None, 52, 66, 77,  None],
              [6, 11,  None,  None,  None, 59, 69,  None, 82]
        ]  # mock: return static card

    @classmethod
    def check_win(cls, card: [[Optional[int]]], marked: [[bool]]) -> bool:
        """
        Checks for a given card if it is marked in a way that counts as a win.
        :param card: A two dimensional array representing the numbers on the card
        :param marked: A two dimensional array, representing what fields are marked already
        :return: If its a win
        """
        # TODO: properly implement and remove mock
        return sum(1 if rc else 0 for r in marked for rc in r) > 3  # mock: return true if more than three marked
