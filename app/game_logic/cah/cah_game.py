from flask_babel import _
from werkzeug.datastructures import ImmutableMultiDict

from app.model.user import User
from app.util.error.bad_user_request_error import BadUserRequestError
from . import decks_service
from ..base.base_game import BaseGame


class CahGame(BaseGame):
    @classmethod
    def get_long_name(cls) -> str:
        return 'Discord With Cards'

    @classmethod
    def get_internal_name(cls) -> str:
        return 'cah'

    @classmethod
    def render_view_new(cls, user: User):
        return cls._render_game_template('new', user=user, deck_names=decks_service.list_decks())

    @classmethod
    def init_new_match(cls, data: ImmutableMultiDict, initiator: User):
        selected_decks_names = data.getlist('selectedDecks', str)
        if len(selected_decks_names) < 1:
            raise BadUserRequestError(_('select_at_least_one_deck'))

        try:
            points_to_win = int(data.get('pointsToWin', -1))
            if points_to_win < 1:
                raise ValueError()
        except ValueError:
            raise BadUserRequestError(_('invalid_winning_point_count'))

        try:
            blank_cards_count = int(data.get('blanksCount', -1))
            if blank_cards_count < 1:
                raise ValueError()
        except ValueError:
            raise BadUserRequestError(_('ínvalid_blank_cards_count'))

        from .cah_match import CahMatch
        match = CahMatch.init_new_match(
            initiator,
            selected_decks_names=selected_decks_names, points_to_win=points_to_win, blank_cards_count=blank_cards_count
        )
        return match
