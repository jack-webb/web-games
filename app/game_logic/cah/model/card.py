import re

from mongoengine import StringField, BooleanField, EmbeddedDocument

re_filter = re.compile('[^a-zA-ZäöüÄÖÜß0-9,;.:\\-_?!"\' $€@%&/²§()=+~#]')


class Card(EmbeddedDocument):
    text = StringField(required=True)
    was_blank = BooleanField(required=True, default=False)

    @classmethod
    def generate(cls, text: str, was_blank=False):
        text = re_filter.sub('', text)
        return cls(
            text=text,
            was_blank=was_blank
        )
