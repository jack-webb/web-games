from mongoengine import EmbeddedDocumentListField, IntField, EmbeddedDocumentField

from ...base.model.base_player_state import BasePlayerState
from .card import Card


class CahPlayerState(BasePlayerState):
    hand = EmbeddedDocumentListField(Card)
    remaining_blank_cards = IntField(min_value=0, required=True)
    selected_card = EmbeddedDocumentField(Card)
