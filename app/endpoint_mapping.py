from flask import Flask
from flask_socketio import SocketIO

from .controller import checks, auth, profile, menu, match, error
from .settings.settings_resolver import Settings


def configure_http_endpoints(app: Flask, settings: Settings):
    # -- Health Check
    app.add_url_rule('/health/', view_func=checks.health_check)

    # -- Error Handling
    error.config_error_pages(app)

    # -- Auth:
    # = '/discord'
    # = '/discord/authorized'
    auth.config_discord_dance(settings, app)
    app.add_url_rule('/auth/', view_func=auth.auth_construct_user)

    # --- User-Details:
    app.add_url_rule('/profile/', view_func=profile.profile_edit)
    app.add_url_rule('/profile/logout/', view_func=profile.profile_logout)
    app.add_url_rule('/profile/', methods=['POST'], view_func=profile.profile_save)

    # --- Menu:
    app.add_url_rule('/', view_func=menu.menu_home)
    app.add_url_rule('/new-game/<internal_game_name>/', view_func=menu.menu_new_game)
    app.add_url_rule('/new-game/<internal_game_name>/', methods=['POST'], view_func=menu.menu_start_game_match)

    # --- Match:
    app.add_url_rule('/match/<match_id>/', view_func=match.match_state)
    app.add_url_rule('/match/<match_id>/results/', view_func=match.match_result)


def configure_ws_endpoints(socketio_app: SocketIO):
    # --- Match:
    socketio_app.on_event('join', namespace='/match', handler=match.match_ws_join)
    socketio_app.on_event('play', namespace='/match', handler=match.match_ws_play)
    socketio_app.on_event('disconnect', namespace='/match', handler=match.match_ws_disconnect)
