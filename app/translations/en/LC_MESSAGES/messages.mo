��    9      �              �     �     �  &   �     �                 	   .     8  ,   J     w     �     �     �  
   �     �     �      �  
   �  
                  2     N     U     d     r     �     �  
   �     �     �  	   �     �     �               $     ?     M     Y     e     ~     �     �     �     �     �     �       	             "     =     Q     b  �  }     (	  2   .	  &   a	     �	     �	     �	     �	     �	     �	  C   �	     6
     C
     P
     b
     t
     �
     �
  ,   �
     �
     �
     �
     �
           5     <     S     p     �     �  
   �  %   �     �     �       ?        O  '   d  !   �     �     �     �  &   �     �     
  &        F     f     s  %   y  	   �     �     �  '   �     �          "   Error _or_join_a_match_via_link at_least_one_deck_needs_to_be_selected back_to_menu back_to_start begin_button bingo_next_number bingo_win blank_cards_count blank_cards_count_needs_to_be_natural_number change_input deck_selection edit_profile edit_profile_title elect_name elect_title filter_list get_name_and_avatar_from_discord hello_user home_title internal_error invalid_type_selected invalid_winning_point_count logout match_settings new_CAH_title new_SOS_title new_bingo_title new_elect_title next_round no_card_deserves_to_win page_not_found play_card points_label points_needs_to_be_gt_one points_to_win possible_solutions report_problem_or_feedback results_title round_label save_button select_at_least_one_deck select_color select_locale select_player_for_vote selected_cards_soon_appear_here show_result_details start_button start_new_match text throw_all vote_results_text waiting_for_card_selection waiting_for_players write_blank_card ínvalid_blank_cards_count Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2020-12-31 20:57+0100
PO-Revision-Date: 2020-04-16 01:15+0200
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: enLanguage_Team: en <LL@li.org> <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.8.0
 Error or join a running match by opening the shared link At least one deck needs to be selected return to the main menu return back to the main menu Let's begin next number Bingo! number of blank cards The number of blank cards needs to be greater than or equal to zero change input Select decks edit your profile Edit your profile [ALPHA] Election [ALPHA] Election Filter the displayed list refresh name and avatar picture from Discord Hello Menu Internal Server Error invalid mode selected Invalid count for winning points logout Settings for the match new Discord With Cards match mew Spillage or Shots match new Bingo match new Election match next round Everything is bad, nobody gets points requested page not found play this card Points The number of points needed to win needs to be greater than one needed points to win Select one of these suggested solutions report a problem or give feedback Match-Results Round save At least one deck needs to be selected Select your favourite color Select your language Vote for the player who fits the best: selected cards will appear here show details start Start a new match by selecting a game Your text throw all cards Results of the Election: Other players are selecting their cards Waiting for players to join custom card Invalid count for blank cards 