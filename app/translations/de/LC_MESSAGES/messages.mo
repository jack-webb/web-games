��    9      �              �     �     �  &   �     �                 	   .     8  ,   J     w     �     �     �  
   �     �     �      �  
   �  
                  2     N     U     d     r     �     �  
   �     �     �  	   �     �     �               $     ?     M     Y     e     ~     �     �     �     �     �     �       	             "     =     Q     b  �  }     (	  K   /	  )   {	     �	     �	  	   �	     �	     �	     �	  @   	
     J
     Z
     l
     ~
     �
     �
     �
  *   �
     �
  
   �
            )   5     _     h     }     �     �     �     �  &   �  %        8     F  J   M     �  &   �  "   �  
          	     )        F     f  D     #   �     �     �  5     	   7     A     P  +   c     �     �  #   �   Error _or_join_a_match_via_link at_least_one_deck_needs_to_be_selected back_to_menu back_to_start begin_button bingo_next_number bingo_win blank_cards_count blank_cards_count_needs_to_be_natural_number change_input deck_selection edit_profile edit_profile_title elect_name elect_title filter_list get_name_and_avatar_from_discord hello_user home_title internal_error invalid_type_selected invalid_winning_point_count logout match_settings new_CAH_title new_SOS_title new_bingo_title new_elect_title next_round no_card_deserves_to_win page_not_found play_card points_label points_needs_to_be_gt_one points_to_win possible_solutions report_problem_or_feedback results_title round_label save_button select_at_least_one_deck select_color select_locale select_player_for_vote selected_cards_soon_appear_here show_result_details start_button start_new_match text throw_all vote_results_text waiting_for_card_selection waiting_for_players write_blank_card ínvalid_blank_cards_count Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2020-12-31 20:57+0100
PO-Revision-Date: 2020-04-16 01:16+0200
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: de
Language-Team: deLanguage_Team: de <LL@li.org> <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.8.0
 Fehler oder tritt einer laufenden Partie bei, indem du den geteilten Link öffnest Mindestens ein Deck muss ausgewählt sein zurück zum Hauptmenü zurück zum Hauptmenü Los gehts nächste Nummer Bingo! Anzahl freier Karten Die Anzahl der freien Karten muss größer oder gleich null sein Eingabe ändern Auswahl der Decks Profil bearbeiten Profil bearbeiten [ALPHA] Die Wahl [ALPHA] Die Wahl angezeigte Liste filtern Name und Avatar-Bild neu von Discord laden Hallo Hauptmenü Interner Fehler Ungültiger Modus ausgewählt Ungültige Anzahl der Punkte zum Gewinnen abmelden Partie Einstellungen neue Discord With Cards Partie neue Spillage or Shots Partie neue Bingo Partie neue Wahl Runde nächste Runde Alles schlecht, niemand bekommt Punkte Angefragte Seite wurde nicht gefunden Karte spielen Punkte Die Anzahl der zum Gewinnen benötigten Punkte muss größer als eins sein benötigte Punkte zum Gewinnen Wähle eine vorgeschlagene Lösung aus Problem melden oder Feedback geben Ergebnisse Runde speichern Mindestens ein Deck muss ausgewählt sein Wähle deine Lieblingsfarbe aus Wähle deine Sprache aus Wähle den Mitspieler, für welchen die Aussage am Meisten zutrifft: ausgewählte Karten erscheinen hier Details anzeigen starten Starte eine neue Partie, indem du ein Spiel aussuchst Dein Text alle wegwerfen Ergebnis der Wahl: die anderen Spieler wählen ihre Karten aus Warte auf Beitritt der Spieler eigene Karte Ungültige Anzahl der freien Karten 