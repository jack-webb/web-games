import logging

from flask import request, url_for, render_template
from mongoengine import DoesNotExist
from werkzeug.datastructures import ImmutableMultiDict
from werkzeug.exceptions import abort
from werkzeug.utils import redirect

from app.game_logic.base.base_match import BaseMatch
from app.game_logic.class_lists import list_match_classes, list_player_state_classes
from app.model.user import User
from app.util.user_guard import ensure_user_is_ready_for_http, ensure_user_is_ready_for_ws

logger = logging.getLogger(__name__)


def _find_match_for_match_id(match_id: str) -> BaseMatch:
    for match_class in list_match_classes():
        try:
            return match_class.objects.get(match_id__exact=match_id)
        except DoesNotExist:
            continue

    # nothing found
    abort(404)


def _find_match_for_player_sid(player_sid: str) -> BaseMatch:
    player_state = None
    for state_class in list_player_state_classes():
        try:
            player_state = state_class.objects.get(sid__exact=player_sid)
        except DoesNotExist:
            continue
    if player_state is None:
        raise DoesNotExist(f'No matching player_state found for sid {player_sid}')

    for match_class in list_match_classes():
        try:
            return match_class.objects.get(player_states=player_state)
        except DoesNotExist:
            continue

    # not found
    raise DoesNotExist(f'No matching player_state found for sid {player_sid}')


@ensure_user_is_ready_for_http
def match_state(user: User, match_id: str) -> str:
    match = _find_match_for_match_id(match_id)
    if not match.get_has_finished():
        return match.render_initial_page(user)
    else:
        return redirect(url_for(match_result.__name__, match_id=match_id))


def match_result(match_id: str) -> str:
    return render_template('games/results.jinja2', match=_find_match_for_match_id(match_id))


@ensure_user_is_ready_for_ws
def match_ws_join(user: User, match_id: str) -> None:
    logger.info(f'got WS event - msg=join match_id={match_id} sid={request.sid} user.discord_id={user.discord_id}')
    _find_match_for_match_id(match_id).handle_join(user)


@ensure_user_is_ready_for_ws
def match_ws_play(user: User, data: ImmutableMultiDict) -> None:
    logger.info(f'got WS event - msg=play sid={request.sid} user.discord_id={user.discord_id} data={str(data)}')
    try:
        _find_match_for_player_sid(request.sid).handle_play(user, data)
    except BaseException as e:
        try:
            from sentry_sdk import capture_exception
            capture_exception(e)
            logging.warning(f'logged error from play event handling to sentry, was: {str(e)}')

        except ModuleNotFoundError:
            logging.error('error during play event handling:')
            logging.exception(e)


@ensure_user_is_ready_for_ws
def match_ws_disconnect(user: User) -> None:
    logger.info(f'got WS event - msg=disconnect sid={request.sid} user.discord_id={user.discord_id}')
    try:
        match = _find_match_for_player_sid(request.sid)
        if not match.get_has_finished():
            match.handle_disconnect(user)
    except DoesNotExist:
        pass  # don't care
