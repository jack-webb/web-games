from .generic_error import GenericError


class BadUserRequestError(GenericError):
    pass
