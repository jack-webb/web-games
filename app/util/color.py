available_colors = [
    '#E50000',
    '#AB0618',
    '#820D15',
    '#8B0D30',
    '#E91E63',
    '#880E73',
    '#AA53E1',
    '#6B1C93',
    '#471574',
    '#5566A0',
    '#2F538D',
    '#113873',
    '#27839A',
    '#055960',
    '#006B50',
    '#105336',
    '#008139',
    '#155b15',
    '#28410B',
    '#C59D13',
    '#FFB900',
    '#FF9100',
    '#F04D0F',
    '#A22703',
]
color_column_widths = [
    i for i in range(2, len(available_colors)//2+1)
    if len(available_colors) % i == 0
]
